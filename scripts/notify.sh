#! /bin/bash

source venv/bin/activate
python src/main.py Faerie Available && XDG_RUNTIME_DIR=/run/user/$(id -u) notify-send --icon="xivlauncher" --app-name="Final Fantasy XIV" "Faerie is open!"
deactivate

