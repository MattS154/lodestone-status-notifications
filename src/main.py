import requests
from bs4 import BeautifulSoup
from  sys import argv, exit
import asyncio

def main(world, status) -> int:
    lodestone = requests.get('https://na.finalfantasyxiv.com/lodestone/worldstatus/')
    
    if (not lodestone.ok):
        return 1

    soup = BeautifulSoup((lodestone.content), 'html.parser')

    world_names = map(lambda el: el.get_text().strip(), soup.find_all("div", class_='world-list__world_name'))
    statuses = map(lambda el: el['data-tooltip'].split(' ')[-1], soup.select('.world-list__create_character i'))

    world_statuses = dict((x, y) for x, y in zip(world_names, statuses))

    return world_statuses[world] != status

if __name__ == "__main__":
    world = argv[1]
    status = argv[2]

    exit(main(world, status))